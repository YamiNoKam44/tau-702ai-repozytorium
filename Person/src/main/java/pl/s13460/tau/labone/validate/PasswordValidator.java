package pl.s13460.tau.labone.validate;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public interface PasswordValidator {
    Pattern VALID_PASSWORD_REGEX =
            Pattern.compile("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[$@$!%*?&])[A-Za-z\\d$@$!%*?&]{8,}$", Pattern.CASE_INSENSITIVE);

    static boolean Validate(String password) {
        return lenghtCheck(password) && correctMatchPassword(password);
    }
    private static boolean lenghtCheck(String password) {
        return password.length() > 8;
    }
    private static boolean correctMatchPassword(String password) {
        Matcher matcher = VALID_PASSWORD_REGEX .matcher(password);
        return matcher.find();
    }
}
