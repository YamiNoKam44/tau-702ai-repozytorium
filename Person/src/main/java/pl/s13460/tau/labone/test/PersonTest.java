package pl.s13460.tau.labone.test;

import org.junit.Assert;
import org.junit.Test;
import pl.s13460.tau.labone.person.*;
import pl.s13460.tau.labone.validate.EmailValidator;
import pl.s13460.tau.labone.validate.PasswordValidator;
import pl.s13460.tau.labone.validate.PeselValidator;

public class PersonTest implements EmailValidator, PasswordValidator, PeselValidator{
    @Test
    public void testIsEmailCorrect() {
        Person person = new Person();
        person.setEmail("des@test.com");
        Assert.assertTrue(EmailValidator.validate(person.getEmail()));
        person.setEmail("dess@@test.com");
        Assert.assertFalse(EmailValidator.validate(person.getEmail()));
    }

    @Test
    public void testIsPasswordCorrect() {
        Person person = new Person();
        person.setPassword("AxQ32DS@sda");
        Assert.assertTrue(PasswordValidator.Validate(person.getPassword()));
        person.setPassword("aaaaaaaaa");
        Assert.assertFalse(PasswordValidator.Validate(person.getPassword()));
    }

    @Test
    public void testIsPeselCorrect() {
        Person person = new Person();
        person.setPesel("12345678910");
        Assert.assertFalse(PeselValidator.validate(person.getPesel()));
        person.setPesel("82013002464");
        Assert.assertTrue(PeselValidator.validate(person.getPesel()));
    }
}
